#![allow(unused)]
use anyhow::Context;
use clap::Args;
use serde::Deserialize;
use std::{net::SocketAddrV4, path::Path};

/// Holds the configuration options of textbin
#[derive(Debug, Deserialize)]
pub struct ConfigFile {
    /// IP address to bind to
    pub listen: Option<String>,
    /// port number to to bind to
    pub port: Option<u16>,
    /// schema returned in URLs to the saved pastebin.
    /// usually `http` or `https`
    #[serde(rename = "emit_scheme")]
    pub scheme: Option<String>,
    /// Maximum size of HTTP request
    pub http_request_size_max: Option<usize>,
    /// Configuration for the storage backend
    pub storage: StorageConfig,
}

/// Hold the Storage backend type
#[derive(Debug, PartialEq, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum StorageConfig {
    /// In-memory backend
    Memory(MemConfig),
    /// Filesystem backend
    Filesystem(FsConfig),
    /// Sqlite3 backend
    Sqlite(SqliteConfig),
}

/// Holds the filesystem specific configuration
#[derive(Default, Clone, PartialEq, Debug, Deserialize)]
pub struct FsConfig {
    /// path to the directory to save pastebins
    pub datadir: std::path::PathBuf,
    /// maximal size of data directory. If no size is given, no limit is applied
    pub max_dir_size: Option<u64>,
}

/// Holds the memory specific configuration
#[derive(Default, PartialEq, Debug, Deserialize)]
pub struct MemConfig {
    pub max_size: Option<usize>,
}

/// Holds the sqlite3 specific configuration
#[derive(Default, PartialEq, Debug, Deserialize)]
pub struct SqliteConfig {
    pub path: std::path::PathBuf,
}

impl ConfigFile {
    /// Create a new Config with default configuration path
    pub fn new() -> anyhow::Result<Self> {
        let config = std::fs::read_to_string(crate::CONFIG_PATH)
            .context("failed to read configuration file")?;
        let config: ConfigFile =
            toml::from_str(&config).context("failed to parse default configuration file")?;
        Ok(config)
    }

    /// Create a new Config from a path
    pub fn from_file(config: impl AsRef<Path>) -> anyhow::Result<Self> {
        let config = std::fs::read_to_string(config)
            .context("failed to read specified configuration file")?;
        let config: ConfigFile = toml::from_str(&config).context("failed to parse file as toml")?;
        Ok(config)
    }

    /// Merge `Config` other into self
    ///
    /// Values from other overwrite values in Self
    pub fn overwrite(&mut self, other: ConfigFile) {
        // TODO: if fields of Config change in the future, this muste be manually extended.
        // There must be a way to macht exhaustively to get compiler warning/errors when fields
        // change.
        self.listen = other.listen;
        self.port = other.port;
        self.storage = other.storage;
    }

    /// Merge arguments from the command line
    pub fn merge_cmdline(&mut self, cli: crate::cli::Cli) {
        if cli.memory {
            // TODO set size if given in cli
            let memconfig = MemConfig { max_size: None };

            self.storage = StorageConfig::Memory(memconfig);
            // if memory is set to true other storage options are ignored
            return;
        }

        if let Some(datadir_cmdline) = cli.datadir {
            // TODO: add cli.size_limit when added
            let fsconfig = FsConfig {
                datadir: datadir_cmdline,
                max_dir_size: None,
            };
            self.storage = StorageConfig::Filesystem(fsconfig)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// returns the "base" configuration
    fn base_config() -> ConfigFile {
        let base_toml = r#"
listen = "127.0.0.1"
port = 9090

[storage]
memory = true
            "#;

        toml::from_str::<ConfigFile>(base_toml).unwrap()
    }

    #[test]
    /// fully replace base_config with another
    fn overwrite_config() {
        let mut base_config = base_config();
        let config = r#"
listen = "0.0.0.0"
port = 1111

[storage.filesystem]
datadir = "datadir/"
            "#;
        let configfile: ConfigFile = toml::from_str(config).unwrap();

        // overwrite base_config
        base_config.overwrite(configfile);

        assert_eq!(base_config.listen, Some("0.0.0.0".to_string()));
        assert_eq!(base_config.port, Some(1111u16));
        assert_eq!(
            base_config.storage,
            StorageConfig::Filesystem(FsConfig {
                datadir: "datadir".into(),
                max_dir_size: None
            })
        );
    }

    #[test]
    /// use datadir from command line
    fn cmdline_custom_datadir() {
        use crate::cli::Cli;

        let mut base_config = base_config();
        let cli = Cli {
            config: None,
            datadir: Some("newpath".into()),
            memory: false,
            max_dir_size: None,
            max_mem_size: None,
        };

        base_config.merge_cmdline(cli);

        assert_eq!(
            base_config.storage,
            StorageConfig::Filesystem(FsConfig {
                datadir: "newpath".into(),
                max_dir_size: None
            })
        );
    }

    #[test]
    /// use memory mode even if datadir is set, too
    /// TODO: look at clap if these options can be mutually exclusive
    fn cmdline_use_memory() {
        use crate::cli::Cli;

        let mut base_config = base_config();
        // even though datadir is set, memory should be used
        let cli = Cli {
            config: None,
            datadir: Some("newpath".into()),
            memory: true,
            max_dir_size: None,
            max_mem_size: None,
        };

        base_config.merge_cmdline(cli);

        assert_eq!(
            base_config.storage,
            StorageConfig::Memory(MemConfig { max_size: None })
        );
    }
}
