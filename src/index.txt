Upload your text files via a POST request to the root of this domain.
Only valid UTF-8 is accepted in the body.


curl -iX POST localhost:3000 --data \"this is some text\"

echo "hello" | curl localhost:3000 -d @-

curl --data-binary @Cargo.toml localhost:3000


This will return a URL at which that data is accessible.
