use crate::storage::{gen_id, PasteBin, Storage};
/// In memory storage
///
/// Your text bins only live as long as the application itself lives.
/// Restarting the application will start with a blank slate
use anyhow::anyhow;
use async_trait::async_trait;
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};
use tracing::error;

/// Holds the textbins in memory
#[derive(Debug, Clone)]
pub struct MemoryBin(Arc<Mutex<HashMap<String, String>>>);

impl MemoryBin {
    // Create a new MemoryBin
    pub fn create() -> Arc<dyn Storage> {
        Arc::new(Self(Arc::new(Mutex::new(HashMap::new()))))
    }
}

#[async_trait]
impl Storage for MemoryBin {
    async fn store(&self, pastebin: PasteBin) -> Result<String, anyhow::Error> {
        if pastebin.expires.is_some() {
            return Err(anyhow!("expire field not supported in memory mode"));
        }
        let id = loop {
            let id = gen_id();

            {
                // keep a lock while checking and inserting data
                let mut bins = self.lock().unwrap();
                if !bins.contains_key(&id) {
                    if let Some(oldval) = bins.insert(id.clone(), pastebin.content) {
                        // this should never happen as we just checked that the key does not
                        // exist while holding the lock
                        error!(?id, ?oldval, "Overrode key with new value");
                    }
                    break id;
                }
            }
        };
        Ok(id)
    }

    async fn load(&self, id: &str) -> Result<Option<String>, anyhow::Error> {
        Ok(self.lock().unwrap().get(id).cloned())
    }
}

impl std::ops::Deref for MemoryBin {
    type Target = Mutex<HashMap<String, String>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
