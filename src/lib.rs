/// default configuration path
const CONFIG_PATH: &str = "./example.config"; // TODO adjust to .config/textbin/config[.toml] or
                                              // ~/.textbin.toml
pub mod cli;
pub mod config;
pub mod error;
pub mod filebin;
pub mod memorybin;
pub mod routes;
pub mod sqlitebin;
pub mod storage;
