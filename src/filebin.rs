use anyhow::{anyhow, Context};
use async_trait::async_trait;
use std::{
    fs::{create_dir_all, remove_file},
    io::ErrorKind,
    path::{Path, PathBuf},
    sync::Arc,
};
use tracing::{debug, info, trace};

use crate::{
    config::FsConfig,
    storage::{gen_id, PasteBin, Storage},
};

#[derive(Debug)]
pub struct FileBin {
    /// path to the directory to save pastebins
    datadir: PathBuf,
    /// maximum size of the data directory
    /// None means no limit
    max_dir_size: Option<u64>,
}

impl FileBin {
    pub fn create(fsconfig: FsConfig) -> Arc<Self> {
        Arc::new(Self {
            datadir: fsconfig.datadir,
            max_dir_size: fsconfig.max_dir_size,
        })
    }
}

#[async_trait]
impl Storage for FileBin {
    async fn store(&self, pastebin: PasteBin) -> Result<String, anyhow::Error> {
        if pastebin.expires.is_some() {
            return Err(anyhow!("expire field not supported in file system mode"));
        }
        // if a size limit for the data dir is set, check if the new upload is exceeding it
        if let Some(size_limit) = self.max_dir_size {
            let pastebin_size = pastebin.content.len() as u64;
            let datadir_size =
                fs_extra::dir::get_size(&self.datadir).map_err(|_| anyhow!("filesystem error"))?;

            trace!(?self.datadir, ?datadir_size, ?size_limit, ?pastebin_size, "size limits");
            if let Some(size_after_upload) = datadir_size.checked_add(pastebin_size) {
                if size_after_upload > size_limit {
                    return Err(anyhow!("reached size limit of data directory"));
                }
            } else {
                tracing::warn!("overflowed u64 in size calculation...lol");
                // we abort as user set a limit and it overflowed
                return Err(anyhow!("filesystem error"));
            }
        }

        // generate unique path id and write content to filesystem
        let id = loop {
            let id = gen_id();
            debug!(?id, "generated id");
            let filename = Path::new(&self.datadir).join(&id);
            if filename.try_exists().unwrap() {
                continue;
            }
            if let Err(err) = std::fs::write(&filename, &pastebin.content) {
                debug!(?err);
                match err.kind() {
                    ErrorKind::NotFound => {
                        info!("datadir seems to be missing. Try recovering");
                        // Might happen if datadir was moved or deleted while the app is running.
                        // We try to recover from it by calling the check_datadir function once
                        // ignoring any errors
                        let _ = check_datadir(&self.datadir);
                        std::fs::write(filename, pastebin.content)
                            .context("could not write file")?;
                        info!("recovered from missing data dir");
                    }
                    _ => return Err(anyhow!("could not write file")),
                }
            }
            break id;
        }; // end loop

        Ok(id)
    }

    async fn load(&self, id: &str) -> Result<Option<String>, anyhow::Error> {
        let filename = Path::new(&self.datadir).join(id);
        if filename.try_exists().is_err() {
            return Ok(None);
        }
        let content = std::fs::read_to_string(filename).context("file system error")?;
        Ok(Some(content))
    }
}

/// Check that the datadir exists and is readable and writable
pub fn check_datadir<P>(datadir: P) -> Result<(), anyhow::Error>
where
    P: Copy + AsRef<Path> + std::convert::AsRef<std::ffi::OsStr> + std::fmt::Debug,
{
    create_dir_all(datadir).context("failed to create all directories")?;

    let testfile = Path::new(&datadir).join("testfile.txt");
    std::fs::write(&testfile, "test").context("failed to write data to testfile")?;

    let content =
        std::fs::read_to_string(&testfile).context("failed to read back from testfile")?;
    remove_file(testfile)?;

    if content != "test" {
        return Err(anyhow!(
            "content read from file does not equal content written to file"
        ));
    }

    info!(?datadir, "verified datadir is readable and writable");
    Ok(())
}
