use crate::storage::{gen_id, PasteBin, Storage};
use anyhow::anyhow;
use async_trait::async_trait;
use chrono::Days;
use sqlx::sqlite::SqliteConnectOptions;
use sqlx::types::chrono::Local;
use sqlx::{Row, SqlitePool};
use std::path::Path;
use tracing::{debug, warn};

/// Store generated path and content
static SQL_STORE: &str = "INSERT INTO textbins (id, content, delete_at) VALUES (?, ?, ?);";

/// Load content for the given path id
static SQL_LOAD: &str = "SELECT * FROM textbins WHERE id = ?;";

#[derive(Debug)]
pub struct SqliteBin {
    /// Handle to the SqlitePool
    pool: SqlitePool,
}

impl SqliteBin {
    pub async fn new(path: impl AsRef<Path>) -> anyhow::Result<Self> {
        let options = SqliteConnectOptions::new()
            .filename(path)
            .create_if_missing(true);

        let pool = SqlitePool::connect_with(options).await?;

        sqlx::migrate!().run(&pool).await?;
        Ok(Self { pool })
    }
}

#[async_trait]
impl Storage for SqliteBin {
    async fn store(&self, pastebin: PasteBin) -> Result<String, anyhow::Error> {
        let expires = pastebin.expires.unwrap_or(30);
        debug!(?expires, "textbin expires days");

        let now = Local::now();
        debug!(?now, "local::now");
        let expires = now.checked_add_days(Days::new(expires.into()));
        debug!(?expires, "expires");

        // retry db operation up to 5 times
        let mut attempts = 1;

        let path = loop {
            let path = gen_id();

            let result = sqlx::query(SQL_STORE)
                .bind(&path)
                .bind(&pastebin.content)
                .bind(&expires) /* TODO NOT NULL constraint violated */
                .execute(&self.pool)
                .await;

            // TODO: pretty naive retry loop. Some errors are fatal and retrying the query won't
            // accomplish anything. Not sure if it's worth doing something about it though.
            match result {
                Ok(_) => break path,
                Err(err) if attempts < 5 => {
                    warn!(?err, "database error inserting textbin");
                    attempts += 1;
                    continue;
                }
                Err(err) => {
                    warn!(?err, "database error inserting textbin");
                    return Err(err.into());
                }
            };
        };

        Ok(path)
    }

    async fn load(&self, id: &str) -> Result<Option<String>, anyhow::Error> {
        let result = sqlx::query(SQL_LOAD)
            .bind(id)
            .fetch_optional(&self.pool)
            .await;

        match result {
            Ok(None) => Ok(None), /* path not found */
            Ok(Some(row)) => Ok(row.get("content")),
            Err(db_err) => {
                warn!(?db_err, "database error");
                return Err(anyhow!("database error"));
            }
        }
    }
}
