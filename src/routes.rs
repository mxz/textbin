use crate::storage;
use crate::{config::ConfigFile, error::AppError, storage::Storage};
use anyhow::Context;
use axum::{
    debug_handler,
    extract::{DefaultBodyLimit, Multipart, Path, State},
    http::StatusCode,
    routing::get,
};
use std::{net::SocketAddr, sync::Arc};
use tracing::debug;

/// maximal size of HTTP request in bytes. Default: 10MiB
const BODYLIMIT: usize = 10_485_760;

/// Contains the application configuration
#[derive(Debug)]
pub struct Config {
    /// socket address of textbin
    pub socket: SocketAddr,
    /// the emitted schema. usually `https` but might be overriden to be http
    pub scheme: String,
    /// maximum size of HTTP request in bytes
    pub http_request_size_max: usize,
}

impl TryFrom<ConfigFile> for Config {
    type Error = anyhow::Error;

    fn try_from(config: ConfigFile) -> Result<Self, Self::Error> {
        let sock = format!(
            "{}:{}",
            config.listen.unwrap_or(String::from("127.0.0.1")),
            config.port.unwrap_or(9090)
        );
        let sock: SocketAddr = sock.parse().context("failed conversion to socket")?;
        Ok(Self {
            socket: sock,
            scheme: config.scheme.unwrap_or(String::from("https")),
            http_request_size_max: config.http_request_size_max.unwrap_or(BODYLIMIT),
        })
    }
}

/// Application State that is accessible to the HTTP handler functions
#[derive(Debug)]
pub struct AppState {
    /// application configuration
    pub config: Config,
    /// handle to the storage provider for saving and loading pastebins
    pub backend: Arc<dyn Storage>,
}

impl AppState {
    /// Create a new AppState with the give application configuration and backend
    pub fn new(config: ConfigFile, backend: Arc<dyn Storage>) -> anyhow::Result<Self> {
        Ok(AppState {
            config: config.try_into()?,
            backend,
        })
    }
}

/// The textbin router that handles all HTTP requests
pub struct BinRouter;

impl BinRouter {
    pub fn create(state: AppState) -> axum::Router {
        debug!(?state.config.http_request_size_max, "maximum http request size");

        axum::Router::new()
            .route("/", get(root).post(add))
            // .route("/list", get(list))
            .route("/:id", get(show))
            .layer(DefaultBodyLimit::max(state.config.http_request_size_max))
            .with_state(Arc::new(state))
    }
}

async fn root() -> &'static str {
    include_str!("index.txt")
}

// async fn list(State(state): State<Arc<dyn Storage>>) -> impl IntoResponse {
//     todo!()
// let mut res = String::from("<ul>");
// for key in state.lock().unwrap().keys() {
//     res.push_str(&format!("<li><a href=\"{HOST}{key}\">{HOST}{key}</a></li>"));
// }
// res.push_str("</ul>");
// Html(res)
// }

#[debug_handler]
async fn show(
    State(state): State<Arc<AppState>>,
    Path(id): Path<String>,
) -> Result<(StatusCode, String), AppError> {
    if let Some(text) = state.backend.load(&id).await? {
        Ok((StatusCode::OK, text))
    } else {
        Ok((StatusCode::NOT_FOUND, String::from("Not Found")))
    }
}

#[debug_handler]
async fn add(
    State(state): State<Arc<AppState>>,
    mut mp: Multipart,
) -> Result<(StatusCode, String), AppError> {
    let mut content = String::new();
    let mut expires = None;

    while let Some(field) = mp.next_field().await?
    /* map_err to StatusCode::BAD_REQUEST ? */
    {
        match field.name() {
            Some("keep") => {
                expires = Some(
                    field
                        .text()
                        .await?
                        .parse::<u16>()
                        .context("\"keep\" must be a number: 1 <= days <= 365")?,
                )
            }
            Some("file") => {
                if !content.is_empty() {
                    // not the first `file` multipart message. Add a separator
                    content.push_str("\n\n==:==\n\n");
                }

                let body = field.text().await?;
                // request size is 2MB by default, so keeping that in RAM is fine
                // TODO: bigger files up to 1GiB?
                content.push_str(&body);
            }
            Some(name) => {
                return Ok((
                    StatusCode::BAD_REQUEST,
                    format!("name \"{}\" not supported", name),
                ))
            }
            None => {
                return Ok((
                    StatusCode::BAD_REQUEST,
                    format!("empty field names not supported"),
                ))
            }
        }
    }

    let length = if content.len() <= 50 {
        content.len()
    } else {
        50
    };
    let data_log = &content[..length];
    let total_len = content.len();
    debug!(?data_log, ?total_len, "start of data");

    let id = state
        .backend
        .store(storage::PasteBin { content, expires })
        .await?;
    debug!(?id, "inserted new key");
    Ok((
        StatusCode::CREATED,
        format!(
            "{}://{}:{}/{}\n",
            state.config.scheme,
            state.config.socket.ip(),
            state.config.socket.port(),
            id
        ),
    ))
}
