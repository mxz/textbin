use anyhow::anyhow;
use clap::Parser;
use std::path::PathBuf;

/// A Simple Pastebin Service
///
/// Command Line Interface
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    /// Path to the configuration file
    #[arg(short, long)]
    pub config: Option<PathBuf>,

    /// Path to the data directory to save pastebins as files
    #[arg(short, long)]
    pub datadir: Option<PathBuf>,

    /// Maximum size of data directory
    #[arg(short = 's', long, group = "size")]
    pub max_dir_size: Option<usize>,

    /// Storage pastebins in-memory
    #[arg(short, long, default_value_t = false)]
    pub memory: bool,

    /// Maximum memory size
    #[arg(short = 'z', long, group = "size")]
    pub max_mem_size: Option<usize>,
}

impl Cli {
    /// Validate command line arguments
    ///
    /// For example, you cannot mix arguments for different storage modes
    pub fn validate(&self) -> anyhow::Result<()> {
        if self.datadir.is_some() && self.memory {
            return Err(anyhow!(
                "conflicting options, filesystem option \"datadir\" and activating memory mode"
            ));
        }

        Ok(())
    }
}
