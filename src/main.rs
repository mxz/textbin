use std::sync::Arc;

use clap::Parser;
use textbin::routes::AppState;
use textbin::sqlitebin::SqliteBin;
use tracing::debug;

use textbin::cli::Cli;
use textbin::config::ConfigFile;
use textbin::config::StorageConfig;
use textbin::filebin;
use textbin::filebin::FileBin;
use textbin::memorybin::MemoryBin;
use textbin::routes::BinRouter;

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let mut config = ConfigFile::new()?;
    debug!(?config, "initial configuration file");

    tracing_subscriber::fmt::init();
    let cli = Cli::parse();
    cli.validate()?;
    debug!(?cli, "commandline args");

    // TODO: either load default config or load config from file.
    if let Some(config_path) = &cli.config {
        let config_extend = ConfigFile::from_file(config_path)?;
        config.overwrite(config_extend);
    }

    config.merge_cmdline(cli);
    debug!(?config, "final app configuration");

    let backend = match config.storage {
        StorageConfig::Memory(_) => {
            tracing::info!("running with in-memory backend");
            #[cfg(target_os = "openbsd")]
            {
                debug!("pledge with memory subsets");
                pledge::pledge("inet stdio", "").unwrap();
            }
            MemoryBin::create()
        }
        StorageConfig::Filesystem(ref fsconfig) => {
            filebin::check_datadir(&fsconfig.datadir)?;
            tracing::info!("running with file backend");

            #[cfg(target_os = "openbsd")]
            {
                debug!(?fsconfig.datadir, "unveil datadir");
                // Use unveil(2) to restrict filesystem access to the data directory.
                // Only allow read/write/create permissions on the data directory
                unveil::unveil(fsconfig.datadir.to_str().unwrap(), "rwc").unwrap();
                // Use pledge(2) to promise that we only use system calls specified in the named subsets.
                // Because we don't allow the `unveil` promise, no further calls to unveil are allowed from
                // this point onwards.
                debug!("pledge now");
                pledge::pledge("rpath wpath cpath inet stdio", "").unwrap();
            }

            FileBin::create(fsconfig.clone())
        }
        StorageConfig::Sqlite(ref config) => Arc::new(SqliteBin::new(&config.path).await?),
    };

    let appstate = AppState::new(config, backend)?;
    let socket = appstate.config.socket;

    let app = BinRouter::create(appstate);

    debug!("listening on {}", socket);
    let server = axum::Server::bind(&socket).serve(app.into_make_service());

    if let Err(err) = server.await {
        eprintln!("server with error: {}", err);
    }
    Ok(())
}
