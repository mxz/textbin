use async_trait::async_trait;
use std::any::Any;

/// Every backend must implement this trait
use rand::{distributions::Alphanumeric, thread_rng, Rng};

/// Every storage provider must implement this functionality
#[async_trait]
pub trait Storage: core::fmt::Debug + Send + Sync + Any + 'static {
    /// Save the given PasteBin
    ///
    /// Returns the id (path) of the stored data
    async fn store(&self, pastebin: PasteBin) -> Result<String, anyhow::Error>;

    /// Show data of the given id
    async fn load(&self, id: &str) -> Result<Option<String>, anyhow::Error>;
}

/// Holds the content of the pastebin and possible metadata
#[derive(Default, Debug)]
pub struct PasteBin {
    /// Content of PasteBin
    pub content: String,
    /// Optional expiry time in days
    pub expires: Option<u16>,
}

/// Generate a unique path id for a pastebin
pub fn gen_id() -> String {
    thread_rng()
        .sample_iter(Alphanumeric)
        .take(6)
        .map(char::from)
        .collect()
}
