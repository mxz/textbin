CREATE TABLE textbins (
	id TEXT PRIMARY KEY,
	content TEXT NOT NULL,
	delete_at TIMESTAMP NOT NULL
);
